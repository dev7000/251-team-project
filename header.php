<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Codeplaza</title>
    <!-- <link rel="stylesheet" type="text/css" href="styles/style.css"> -->
    <link rel="stylesheet" type="text/css" href="styles/reset.css">
    <link rel="stylesheet" type="text/css" href="styles/header.css">
    <link rel="stylesheet" type="text/css" href="styles/index.css">
    <link rel="stylesheet" type="text/css" href="styles/signup.css">
    <link rel="stylesheet" type="text/css" href="styles/editor.css">
</head>
<body>
    <header>
        <nav>
            <div class="main-wrapper">
                <ul>
                    <?php
                        if ($_SESSION['u_id']) {
                            echo '<li><a href="index.php">'.$_SESSION['username'].'</a></li>';
                        }
                        else {
                            echo '<li><a href="index.php">Home</a></li>';
                        }
                    ?>
                </ul>
                <div class="nav-login">
                    <?php
                        if ($_SESSION['u_id']) {
                            echo '<form action="includes/logout.inc.php" method="POST">
                            <button type="submit" name="submit">Logout</button>
                            </form>';
                        }
                        else {
                            echo '<form action="includes/login.inc.php" method="POST">
                            <input type="text" placeholder="username/email" name="uid">
                            <input type="password" placeholder="password" name="pwd">
                            <button type="submit" name="submit">Login</button>
                            </form>
                            <a href="signup.php">Sign up</a>';
                        }
                    ?>    
                </div>
            </div>
        </nav>
    </header>