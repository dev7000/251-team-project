<?php
    if (isset($_POST['run'])) {
        mkdir("temp");

        $codetext = NULL;
        $codefile = NULL;

        $inputtext = NULL;
        $inputfile = NULL;

        $errorstext = NULL;
        $outputtext = NULL;

        switch($_POST['language'])
        {
            case "c":
            {
                include_once("./processing/c.php");
                break;
            }
            case "cpp":
            {
                include_once("./processing/cplusplus.php");
                break;
            }
            case "python2.7":
            {
                include_once("./processing/python2.php");
                break;
            }
            case "python3.5":
            {
                include_once("./processing/python3.php");
                break;
            }
        }
        shell_exec("rm -rf temp");
    }
?>