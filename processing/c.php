<?php
    $codetext = $_POST["code"];
    $codefile = fopen("temp/main.c", "w+");
    fwrite($codefile,$codetext);
    fclose($codefile);

    $inputtext = $_POST["input"];
    $inputfile = fopen("temp/input.txt","w+");
    fwrite($inputfile,$inputtext);
    fclose($inputfile);

    shell_exec("gcc -lm temp/main.c -o temp/a.out 2> temp/errors.txt");
    shell_exec("chmod 777 -R temp");
    shell_exec("temp/a.out < temp/input.txt > temp/output.txt");

    $errorstext = file_get_contents("temp/errors.txt");
    $outputtext = file_get_contents("temp/output.txt");
?>