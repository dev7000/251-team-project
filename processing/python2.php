<?php
    $codetext = $_POST["code"];
    $codefile = fopen("temp/main.py", "w+");
    fwrite($codefile,$codetext);
    fclose($codefile);

    $inputtext = $_POST["input"];
    $inputfile = fopen("temp/input.txt","w+");
    fwrite($inputfile,$inputtext);
    fclose($inputfile);

    shell_exec("python2.7 temp/main.py < temp/input.txt > temp/output.txt 2> temp/errors.txt");
    shell_exec("chmod 777 -R temp");
    $errorstext = file_get_contents("temp/errors.txt");
    $outputtext = file_get_contents("temp/output.txt");
?>