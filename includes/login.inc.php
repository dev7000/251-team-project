<?php

session_start();

if (isset($_POST['submit'])) {
    
    include_once 'dbh.inc.php';
    
    $uid = mysqli_real_escape_string($connect, $_POST['uid']);
    $pwd = mysqli_real_escape_string($connect, $_POST['pwd']);
    
    //Error handling
    //Check if inputs are empty
    if (empty($uid) || empty($pwd)) {
        header("Location: ../index.php?login=empty");
        exit();
    } else {
        $sql = "SELECT * FROM users WHERE user_uid='$uid' OR user_email='$uid'";
        $result = mysqli_query($connect, $sql);
        $resultCheck = mysqli_num_rows($result);
        if ($resultCheck < 1) {
            header("Location: ../index.php?login=error");
            exit();
        } else {
            if ($row = mysqli_fetch_assoc($result)) {
                //Dehashing the stored password
                $hashedPwdCheck = password_verify($pwd, $row['user_pwd']);
                if ($hashedPwdCheck == false) {
                    header("Location: ../index.php?login=error");
                    exit();
                } elseif($hashedPwdCheck == true) {
                    //User login here
                    $_SESSION['u_id'] = $row['user_id'];
                    $_SESSION['u_first'] = $row['user_first'];
                    $_SESSION['u_last'] = $row['user_last'];
                    $_SESSION['u_uid'] = $row['user_uid'];
                    $_SESSION['u_pwd'] = $row['user_pwd'];
                    $_SESSION['username'] = $uid;
                    header("Location: ../index.php?login=success");
                    exit();
                }
            }
        }
    }
}   else {
    header("Location: ../index.php?login=error");
    exit();
}