<?php

if (isset($_POST['creator_submit'])) {

    $dbServername = "localhost";
    $dbUsername = "root";
    $dbPassword = "rootpassword";
    $dbName = "login";
    
    $connect2 = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);

    $q_name = mysqli_real_escape_string($connect2, $_POST['q_name']);
    $q_content = mysqli_real_escape_string($connect2, $_POST['content']);
    $test_input = mysqli_real_escape_string($connect2, $_POST['testcase_input']);
    $test_output = mysqli_real_escape_string($connect2, $_POST['testcase_output']);

    //add question to the database
    $sql = "INSERT INTO questions (question_name, question_content, question_testcase_input, question_testcase_output) VALUES ('$q_name', '$q_content', '$test_input', '$test_output')";
    $result = mysqli_query($connect2, $sql);
    header("Location: ../index.php?question=added");
    exit();

} else {
    header("Location: ../creatorpage.php?invalid");
    exit();
}