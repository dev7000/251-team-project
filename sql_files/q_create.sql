CREATE TABLE questions (
    question_id int(11) not null AUTO_INCREMENT PRIMARY KEY,
    question_name varchar(256) not null,
    question_content text,
    question_testcase_input text,
    question_testcase_output text
);