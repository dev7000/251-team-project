<?php
    include_once("./processing.php");
?>

<div class="editor">
    <form id="editorspace" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
        <select name="language" id="language">
            <option value="c" <?php echo (isset($_POST['language']) && $_POST['language'] == 'c') ? 'selected="selected"' : ''; ?>>C</option>
            <option value="cpp" <?php echo (isset($_POST['language']) && $_POST['language'] == 'cpp') ? 'selected="selected"' : ''; ?>>C++</option>
            <!-- <option value="java">Java</option> -->
            <option value="python2.7" <?php echo (isset($_POST['language']) && $_POST['language'] == 'python2.7') ? 'selected="selected"' : ''; ?>>Python</option>
            <option value="python3.5" <?php echo (isset($_POST['language']) && $_POST['language'] == 'python3.5') ? 'selected="selected"' : ''; ?>>Python3</option>
        </select>
        <textarea name="code" id="code" placeholder="Your code goes here.."><?php if (isset($_POST['code'])) { echo htmlspecialchars($_POST['code']); } ?></textarea>
        <input type="reset">
        <input type="submit" name="run" value="Run">
    <!-- </form> -->
        <div class="tab">
            <button type="button" class="tablinks" onclick="opentab(event, 'Input')">Input</button>
            <button type="button" class="tablinks" id="outbutton" onclick="opentab(event, 'Output')">Output</button>
            <button type="button" class="tablinks" onclick="opentab(event, 'Errors')">Error log</button>
        </div>
        <div id="Input" class="tabcontent">
            <textarea name="input" id="input" placeholder="Your input goes here.."><?php if (isset($_POST['input'])) { echo htmlspecialchars($_POST['input']); } ?></textarea>
        </div>
        <div id="Output" class="tabcontent">
            <textarea name="output" id="output" placeholder="Output here.." readonly><?php if (isset($outputtext)) { echo htmlspecialchars($outputtext); } ?></textarea>
        </div>
        <div id="Errors" class="tabcontent">
            <textarea name="errors" id="errors" placeholder="Errors here.." readonly><?php if (isset($errorstext)) { echo htmlspecialchars($errorstext); } ?></textarea>
        </div>
    </form>
</div>

<script type="text/javascript">
    function opentab(evt, tabName) {
        // Declare all variables
        var i, tabcontent, tablinks;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        // Show the current tab, and add an "active" class to the button that opened the tab
        document.getElementById(tabName).style.display = "block";
        evt.currentTarget.className += " active";
    } 

    // function loader() {
    //     document.body.onload = document.getElementsById('outbutton').click();
    // }
</script>
