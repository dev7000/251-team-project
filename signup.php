<?php
    include_once 'header.php';
?>
<section class="main-container">
    <div class="main-wrappper">
        <h2>Sign up</h2>
        <form class="signup-form" action="includes/signup.inc.php" method="POST">
            <input type="text" placeholder="First name" name="first">
            <input type="text" placeholder="Last name" name="last">
            <input type="text" placeholder="Email" name="email">
            <input type="text" placeholder="username" name="uid">
            <input type="password" placeholder="password" name="pwd">
            <button type="submit" name="submit">Sign up</button>
        </form>
    </div>
</section>
<?php
    include_once 'footer.php';
?>